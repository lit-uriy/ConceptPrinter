/*
 * Copyright (c) 2018-2019 Viktor Kireev
 * Distributed under the MIT License
 */

import qbs

UplConsoleApplication {
    Depends { name: "Upl.Core" }
}
